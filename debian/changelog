dh-make-raku (0.8) unstable; urgency=medium

  [ Timo Paulssen ]
  * Fix generated filenamemangle for watch file.
  * debian/control: Add missing dependency on Config::INI
  * debian/control: Add dependency to GitLab::API::V4
  * control: declare compliance with policy 4.7.0

 -- Dominique Dumont <dod@debian.org>  Wed, 16 Oct 2024 18:48:37 +0200

dh-make-raku (0.7) unstable; urgency=medium

  * dh-make-raku: improve doc
  * setup_git: setup remote url if --git option is used
  * DhMakeRaku: call apply_fixes on dpkg files
  * DhMakeRaku: fix parsing of raku dependencies

 -- Dominique Dumont <dod@debian.org>  Wed, 01 Nov 2023 16:26:55 +0100

dh-make-raku (0.6) unstable; urgency=medium

  * dh-make-raku: remove use of obsolete module
  * GitLab: fix parsing of .git-credentials
  * GitLab: fix error check on salsa token
  * upgrade: check if merge is necessary
  * upgrade: update debian/changelog if necessary
  * DhMakeRaku: die if quilt patch apply fails
  * control: declare compliance with policy 4.6.2

 -- Dominique Dumont <dod@debian.org>  Sun, 29 Oct 2023 16:19:55 +0100

dh-make-raku (0.5) unstable; urgency=medium

  [ Debian Janitor ]
  * Move libmodule-build-perl from Build-Depends-Indep to Build-Depends.
  * Move source package lintian overrides to debian/source.
  * Set field Upstream-Name in debian/copyright.

  [ Dominique Dumont ]
  * do not build-dep on prove6 automatically
  * Do not set dependency when this one has hints
  * no longer updates dependencies in raku package, drop the recommended
    layout for modules.
  * upstream remote is now named upstream-git
  * handle upgrade of a new version with --upgrade option

 -- Dominique Dumont <dod@debian.org>  Sun, 06 Nov 2022 18:41:30 +0100

dh-make-raku (0.4) unstable; urgency=medium

  * DhMakeRaku:
    * add Build-Depends from META6
    * generate gbp.conf tag pattern from upstream tag
    * do not write gbp.conf without a git tag
    * setup file to install module docs
    * remove .git from Homepage and Source URL
  * dh-make-raku: update doc
  * control: depends on libconfig-model-dpkg-perl >= 2.164

 -- Dominique Dumont <dod@debian.org>  Fri, 09 Sep 2022 14:49:15 +0200

dh-make-raku (0.3) unstable; urgency=medium

  * DhMakeRaku:
    * setup raku dependencies for star modules
    * Set arch any for raku modules
    * setup debian/README.source
    * add BD on prove6 when t/ is found
    * setup rm-shebang patch when needed
    * handle special Config module
    * handle version upgrade of a dependency
  * patch setup:
    * set applied-upstream to not-needed
    * do not run quilt pop when there's no patches
    * configure quilt if ~/.quiltrc is not found
  * doc: add requirements section
  * add lintian-override for patch warning
  * control: declare compliance with policy 4.6.1
  * control: depend on quilt

 -- Dominique Dumont <dod@debian.org>  Tue, 28 Jun 2022 19:23:21 +0200

dh-make-raku (0.2) unstable; urgency=medium

  * rename upstream's default branch to upstream branch
  * set correct version in changelog
  * add missing git tags
  * DhMakeRaku: install files from examples directory

 -- Dominique Dumont <dod@debian.org>  Fri, 17 Jun 2022 18:59:00 +0200

dh-make-raku (0.1.1) unstable; urgency=medium

  * create Raku module project on Salsa
  * automatically add dependency in raku package for Rakudo Star modules

 -- Dominique Dumont <dod@debian.org>  Sun, 05 Jun 2022 10:31:07 +0200

dh-make-raku (0.0.1) unstable; urgency=medium

  * Initial release

 -- Dominique Dumont <dod@debian.org>  Sun, 15 May 2022 18:04:33 +0200
