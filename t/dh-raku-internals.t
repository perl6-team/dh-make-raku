use strict;
use warnings;
use v5.20;

use Test::More;
use Path::Tiny;
use Config::Model qw/cme/;

use DhMakeRaku;

subtest "update debian copyright" => sub {
    my $email = $ENV{DEBEMAIL} = 'jb@team.org';
    my $name = $ENV{DEBFULLNAME} = 'Joe Bar';
    my @l = localtime;
    my $year = $l[5]+1900;
    my $oldyear = $year - 1;

    my @tests = (
        [ __LINE__, undef , "$year, $name <$email>" ],
        [ __LINE__, '2015, Marcel Mézigues <marcel@nowhere>' => "{}\n$year, $name <$email>" ],
        [ __LINE__, "2015, $name <$email>" => "2015, $year, $name <$email>" ],
        [ __LINE__, "2015, $oldyear, $name <$email>" => "2015, $oldyear-$year, $name <$email>" ],
        [ __LINE__, "2015-$oldyear, $name <$email>" => "2015-$year, $name <$email>" ],
    );

    foreach my $test( @tests ) {
        $test->[2] =~ s/{}/$test->[1]/e;
        my $new_cop = DhMakeRaku::update_debian_copyright($test->[1]);
        is($new_cop, $test->[2],"check copyright entry line $test->[0]");
    }
};

subtest "get_description" => sub {
    my $package = 'raku-file-find';
    my $target = path("t/samples/$package");
    chdir($target);

    my $instance = cme('dpkg');
    my $root = $instance->config_root;

    my $desc = $root->grab("control binary:$package Description");
    DhMakeRaku::setup_control ($root, $target, 'dummy-git', 'raku-file-find');

    like($desc->fetch, qr/please fill/i, "default value when no description is found");

    $desc->store("Dummy description");

    DhMakeRaku::setup_control ($root, $target, 'dummy-git', 'raku-file-find');

    like($desc->fetch, qr/dummy description/i, "User value not clobbered when no description is found");
};

subtest "get_watch_content" => sub {
    my $package = 'raku-file-find';
    my @tests = (
         __LINE__, 'https://github.com/plop.git' , qr!com/plop/tags! ,
         __LINE__, 'https://gitother.com/plop.git' , qr!plop\.git! ,
    );

    while (my ($line, $git, $exp) = splice @tests, 0,3 ) {
        my $watch = DhMakeRaku::get_watch_content($git, $package);
        like($watch, $exp, "check $git watch file (from line $line)");
    }
};

subtest "translate dependencies" => sub{
    my @tests = (
        __LINE__, 'Hash::Merge:version<1.0.1>' => 'raku-hash-merge (>= 1.0.1)',
        __LINE__, "Log:auth<cpan:TYIL>:version<0.3.0>" => 'raku-log (>= 0.3.0)',
        __LINE__, "JSON::Marshal:ver<0.0.25+>" => 'raku-json-marshal (>= 0.0.25)',
    );

    while (my ($line, $raku_dep, $deb_dep, $exp) = splice @tests, 0,3 ) {
        my $res = DhMakeRaku::raku_dep_to_debian_dep($raku_dep);
        is($res, $deb_dep, "translater raku dep $raku_dep (from line $line)");
    }
};

done_testing;
